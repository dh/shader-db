[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define texture1D texture
#define texture2D texture
#define texture3D texture
#define inf 1e32 
#define USE_GPU_SHADER_CREATE_INFO
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool1 a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define static
#  define inline
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#  define float2 vec2
#  define float3 vec3
#  define float4 vec4
#  define float4x4 mat4
#  define int2 ivec2
#  define int3 ivec3
#  define int4 ivec4
#  define uint2 uvec2
#  define uint3 uvec3
#  define uint4 uvec4
#  define bool1 bool
#  define bool2 bvec2
#  define bool3 bvec3
#  define bool4 bvec4

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  ifdef __cplusplus
#    include  BLI_float4x4.hh 
#    include  BLI_math_vec_types.hh 
using blender::float2;
using blender::float3;
using blender::float4;
using blender::float4x4;
using blender::int2;
using blender::int3;
using blender::int4;
using blender::uint2;
using blender::uint3;
using blender::uint4;
using bool1 = int;
using bool2 = blender::int2;
using bool3 = blender::int3;
using bool4 = blender::int4;

#  else /* C */
typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float float4x4[4][4];
typedef int int2[2];
typedef int int3[2];
typedef int int4[4];
typedef uint uint2[2];
typedef uint uint3[3];
typedef uint uint4[4];
typedef int bool1;
typedef int bool2[2];
typedef int bool3[2];
typedef int bool4[4];
#  endif

#endif
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

#ifndef GPU_SHADER
#  include  GPU_shader_shared_utils.h 
#endif

struct OCIO_GPUCurveMappingParameters {
  /* Curve mapping parameters
   *
   * See documentation for OCIO_CurveMappingSettings to get fields descriptions.
   * (this ones pretty much copies stuff from C structure.)
   */
  float4 mintable;
  float4 range;
  float4 ext_in_x;
  float4 ext_in_y;
  float4 ext_out_x;
  float4 ext_out_y;
  float4 first_x;
  float4 first_y;
  float4 last_x;
  float4 last_y;
  float4 black;
  float4 bwmul;
  int lut_size;
  int use_extend_extrapolate;
  int _pad0;
  int _pad1;
};

struct OCIO_GPUParameters {
  float dither;
  float scale;
  float exponent;
  bool1 use_predivide;
  bool1 use_overlay;
  int _pad0;
  int _pad1;
  int _pad2;
};

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image_texture;
layout(binding = 1) uniform sampler2D overlay_texture;
layout(binding = 0, std140) uniform parameters { OCIO_GPUParameters _parameters; };
layout(binding = 3) uniform sampler2D to_display_lut1d_0Sampler;
#define parameters (_parameters)

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;


/* Inputs. */
layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 texCoord;

/* Interfaces. */
out OCIO_Interface{
  smooth vec2 texCoord_interp;
};


void main()
{
  gl_Position = ModelViewProjectionMatrix * vec4(pos.xy, 0.0f, 1.0f);
  texCoord_interp = texCoord;
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define texture1D texture
#define texture2D texture
#define texture3D texture
#define inf 1e32 
#define USE_GPU_SHADER_CREATE_INFO
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup gpu
 *
 * Glue definition to make shared declaration of struct & functions work in both C / C++ and GLSL.
 * We use the same vector and matrix types as Blender C++. Some math functions are defined to use
 * the float version to match the GLSL syntax.
 * This file can be used for C & C++ code and the syntax used should follow the same rules.
 * Some preprocessing is done by the GPU back-end to make it GLSL compatible.
 *
 * IMPORTANT:
 * - Always use `u` suffix for enum values. GLSL do not support implicit cast.
 * - Define all values. This is in order to simplify custom pre-processor code.
 * - (C++ only) Always use `uint32_t` as underlying type (`enum eMyEnum : uint32_t`).
 * - (C only) do NOT use the enum type inside UBO/SSBO structs and use `uint` instead.
 * - Use float suffix by default for float literals to avoid double promotion in C++.
 * - Pack one float or int after a vec3/ivec3 to fulfill alignment rules.
 *
 * NOTE: Due to alignment restriction and buggy drivers, do not try to use mat3 inside structs.
 * NOTE: (UBO only) Do not use arrays of float. They are padded to arrays of vec4 and are not worth
 * it. This does not apply to SSBO.
 *
 * IMPORTANT: Do not forget to align mat4, vec3 and vec4 to 16 bytes, and vec2 to 8 bytes.
 *
 * NOTE: You can use bool type using bool1 a int boolean type matching the GLSL type.
 */

#ifdef GPU_SHADER
#  define BLI_STATIC_ASSERT_ALIGN(type_, align_)
#  define BLI_STATIC_ASSERT_SIZE(type_, size_)
#  define static
#  define inline
#  define cosf cos
#  define sinf sin
#  define tanf tan
#  define acosf acos
#  define asinf asin
#  define atanf atan
#  define floorf floor
#  define ceilf ceil
#  define sqrtf sqrt
#  define expf exp

#  define float2 vec2
#  define float3 vec3
#  define float4 vec4
#  define float4x4 mat4
#  define int2 ivec2
#  define int3 ivec3
#  define int4 ivec4
#  define uint2 uvec2
#  define uint3 uvec3
#  define uint4 uvec4
#  define bool1 bool
#  define bool2 bvec2
#  define bool3 bvec3
#  define bool4 bvec4

#else /* C / C++ */
#  pragma once

#  include  BLI_assert.h 

#  ifdef __cplusplus
#    include  BLI_float4x4.hh 
#    include  BLI_math_vec_types.hh 
using blender::float2;
using blender::float3;
using blender::float4;
using blender::float4x4;
using blender::int2;
using blender::int3;
using blender::int4;
using blender::uint2;
using blender::uint3;
using blender::uint4;
using bool1 = int;
using bool2 = blender::int2;
using bool3 = blender::int3;
using bool4 = blender::int4;

#  else /* C */
typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float float4x4[4][4];
typedef int int2[2];
typedef int int3[2];
typedef int int4[4];
typedef uint uint2[2];
typedef uint uint3[3];
typedef uint uint4[4];
typedef int bool1;
typedef int bool2[2];
typedef int bool3[2];
typedef int bool4[4];
#  endif

#endif
/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2022 Blender Foundation. All rights reserved. */

#ifndef GPU_SHADER
#  include  GPU_shader_shared_utils.h 
#endif

struct OCIO_GPUCurveMappingParameters {
  /* Curve mapping parameters
   *
   * See documentation for OCIO_CurveMappingSettings to get fields descriptions.
   * (this ones pretty much copies stuff from C structure.)
   */
  float4 mintable;
  float4 range;
  float4 ext_in_x;
  float4 ext_in_y;
  float4 ext_out_x;
  float4 ext_out_y;
  float4 first_x;
  float4 first_y;
  float4 last_x;
  float4 last_y;
  float4 black;
  float4 bwmul;
  int lut_size;
  int use_extend_extrapolate;
  int _pad0;
  int _pad1;
};

struct OCIO_GPUParameters {
  float dither;
  float scale;
  float exponent;
  bool1 use_predivide;
  bool1 use_overlay;
  int _pad0;
  int _pad1;
  int _pad2;
};

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image_texture;
layout(binding = 1) uniform sampler2D overlay_texture;
layout(binding = 0, std140) uniform parameters { OCIO_GPUParameters _parameters; };
layout(binding = 3) uniform sampler2D to_display_lut1d_0Sampler;
#define parameters (_parameters)

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;


/* Interfaces. */
in OCIO_Interface{
  smooth vec2 texCoord_interp;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;

/* Blender OpenColorIO implementation */

/* -------------------------------------------------------------------- */
/** \name Curve Mapping Implementation
 * \{ */

#ifdef USE_CURVE_MAPPING

float read_curve_mapping(int table, int index)
{
  return texelFetch(curve_mapping_texture, index, 0)[table];
}

float curvemap_calc_extend(int table, float x, vec2 first, vec2 last)
{
  if (x <= first[0]) {
    if (curve_mapping.use_extend_extrapolate == 0) {
      /* horizontal extrapolation */
      return first[1];
    }
    else {
      float fac = (curve_mapping.ext_in_x[table] != 0.0) ?
                      ((x - first[0]) / curve_mapping.ext_in_x[table]) :
                      10000.0;
      return first[1] + curve_mapping.ext_in_y[table] * fac;
    }
  }
  else if (x >= last[0]) {
    if (curve_mapping.use_extend_extrapolate == 0) {
      /* horizontal extrapolation */
      return last[1];
    }
    else {
      float fac = (curve_mapping.ext_out_x[table] != 0.0) ?
                      ((x - last[0]) / curve_mapping.ext_out_x[table]) :
                      -10000.0;
      return last[1] + curve_mapping.ext_out_y[table] * fac;
    }
  }
  return 0.0;
}

float curvemap_evaluateF(int table, float value)
{
  float mintable_ = curve_mapping.mintable[table];
  float range = curve_mapping.range[table];
  float mintable = 0.0;
  int CM_TABLE = curve_mapping.lut_size - 1;

  float fi;
  int i;

  /* index in table */
  fi = (value - mintable) * range;
  i = int(fi);

  /* fi is table float index and should check against table range i.e. [0.0 CM_TABLE] */
  if (fi < 0.0 || fi > float(CM_TABLE)) {
    return curvemap_calc_extend(table,
                                value,
                                vec2(curve_mapping.first_x[table], curve_mapping.first_y[table]),
                                vec2(curve_mapping.last_x[table], curve_mapping.last_y[table]));
  }
  else {
    if (i < 0) {
      return read_curve_mapping(table, 0);
    }
    if (i >= CM_TABLE) {
      return read_curve_mapping(table, CM_TABLE);
    }
    fi = fi - float(i);
    float cm1 = read_curve_mapping(table, i);
    float cm2 = read_curve_mapping(table, i + 1);
    return mix(cm1, cm2, fi);
  }
}

vec4 curvemapping_evaluate_premulRGBF(vec4 col)
{
  col.rgb = (col.rgb - curve_mapping.black.rgb) * curve_mapping.bwmul.rgb;

  vec4 result;
  result.r = curvemap_evaluateF(0, col.r);
  result.g = curvemap_evaluateF(1, col.g);
  result.b = curvemap_evaluateF(2, col.b);
  result.a = col.a;
  return result;
}

#endif /* USE_CURVE_MAPPING */

/** \} */

/* -------------------------------------------------------------------- */
/** \name Dithering
 * \{ */

/* Using a triangle distribution which gives a more final uniform noise.
 * See Banding in Games:A Noisy Rant(revision 5) Mikkel Gjøl, Playdead (slide 27) */
/* GPUs are rounding before writing to framebuffer so we center the distribution around 0.0. */
/* Return triangle noise in [-1..1[ range */
float dither_random_value(vec2 co)
{
  /* Original code from https://www.shadertoy.com/view/4t2SDh */
  /* Uniform noise in [0..1[ range */
  float nrnd0 = fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
  /* Convert uniform distribution into triangle-shaped distribution. */
  float orig = nrnd0 * 2.0 - 1.0;
  nrnd0 = orig * inversesqrt(abs(orig));
  nrnd0 = max(-1.0, nrnd0); /* Removes nan's */
  return nrnd0 - sign(orig);
}

vec2 round_to_pixel(sampler2D tex, vec2 uv)
{
  vec2 size = vec2(textureSize(tex, 0));
  return floor(uv * size) / size;
}

vec4 apply_dither(vec4 col, vec2 uv)
{
  col.rgb += dither_random_value(uv) * 0.0033 * parameters.dither;
  return col;
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Main Processing
 * \{ */

/* Prototypes: Implementation is generaterd and defined after. */
vec4 OCIO_to_scene_linear(vec4 pixel);
vec4 OCIO_to_display(vec4 pixel);

vec4 OCIO_ProcessColor(vec4 col, vec4 col_overlay)
{
#ifdef USE_CURVE_MAPPING
  col = curvemapping_evaluate_premulRGBF(col);
#endif

  if (parameters.use_predivide) {
    if (col.a > 0.0 && col.a < 1.0) {
      col.rgb *= 1.0 / col.a;
    }
  }

  /* NOTE: This is true we only do de-premul here and NO premul
   *       and the reason is simple -- opengl is always configured
   *       for straight alpha at this moment
   */

  /* Convert to scene linear (usually a no-op). */
  col = OCIO_to_scene_linear(col);

  /* Apply exposure in scene linear. */
  col.rgb *= parameters.scale;

  /* Convert to display space. */
  col = OCIO_to_display(col);

  /* Blend with overlay in UI colorspace.
   *
   * UI colorspace here refers to the display linear color space,
   * i.e: The linear color space w.r.t. display chromaticity and radiometry.
   * We separate the colormanagement process into two steps to be able to
   * merge UI using alpha blending in the correct color space. */
  if (parameters.use_overlay) {
    col.rgb = pow(col.rgb, vec3(parameters.exponent * 2.2));
    col = clamp(col, 0.0, 1.0);
    col *= 1.0 - col_overlay.a;
    col += col_overlay; /* Assumed unassociated alpha. */
    col.rgb = pow(col.rgb, vec3(1.0 / 2.2));
  }
  else {
    col.rgb = pow(col.rgb, vec3(parameters.exponent));
  }

  if (parameters.dither > 0.0) {
    vec2 noise_uv = round_to_pixel(image_texture, texCoord_interp.st);
    col = apply_dither(col, noise_uv);
  }

  return col;
}

/** \} */

void main()
{
  vec4 col = texture(image_texture, texCoord_interp.st);
  vec4 col_overlay = texture(overlay_texture, texCoord_interp.st);

  fragColor = OCIO_ProcessColor(col, col_overlay);
}

// Declaration of the OCIO shader function

vec4 OCIO_to_scene_linear(vec4 inPixel)
{
  vec4 outColor = inPixel;

  return outColor;
}


// Declaration of all variables

//iform sampler2D to_display_lut1d_0Sampler;

// Declaration of all helper methods

vec2 to_display_lut1d_0_computePos(float f)
{
  float dep = clamp(f, 0.0, 1.0) * 65560.;
  vec2 retVal;
  retVal.y = float(int(dep / 4095.));
  retVal.x = dep - retVal.y * 4095.;
  retVal.x = (retVal.x + 0.5) / 4096.;
  retVal.y = (retVal.y + 0.5) / 17.;
  return retVal;
}

// Declaration of the OCIO shader function

vec4 OCIO_to_display(vec4 inPixel)
{
  vec4 outColor = inPixel;
  
  // Add Matrix processing
  
  {
    vec4 res = vec4(outColor.rgb.r, outColor.rgb.g, outColor.rgb.b, outColor.a);
    res = vec4(0.0247098133, 0.0247098133, 0.0247098133, 1.) * res;
    res = vec4(0.000239065543, 0.000239065543, 0.000239065543, 0.) + res;
    outColor.rgb = vec3(res.x, res.y, res.z);
    outColor.a = res.w;
  }
  
  // Add LUT 1D processing for to_display_lut1d_0
  
  {
    outColor.r = texture2D(to_display_lut1d_0Sampler, to_display_lut1d_0_computePos(outColor.r)).r;
    outColor.g = texture2D(to_display_lut1d_0Sampler, to_display_lut1d_0_computePos(outColor.g)).r;
    outColor.b = texture2D(to_display_lut1d_0Sampler, to_display_lut1d_0_computePos(outColor.b)).r;
  }

  return outColor;
}


