[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define blender_srgb_to_framebuffer_space(a) a 
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D glyph;

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform bool srgbTarget;


/* Inputs. */
layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 col;
layout(location = 2) in ivec2 glyph_size;
layout(location = 3) in int offset;

/* Interfaces. */
out text_iface{
  flat vec4 color_flat;
  noperspective vec2 texCoord_interp;
  flat int glyph_offset;
  flat ivec2 glyph_dim;
  flat int interp_size;
};


void main()
{
  color_flat = col;
  glyph_offset = offset;
  glyph_dim = abs(glyph_size);
  interp_size = int(glyph_size.x < 0) + int(glyph_size.y < 0);

  /* Quad expension using instanced rendering. */
  float x = float(gl_VertexID % 2);
  float y = float(gl_VertexID / 2);
  vec2 quad = vec2(x, y);

  vec2 interp_offset = float(interp_size) / abs(pos.zw - pos.xy);
  texCoord_interp = mix(-interp_offset, 1.0 + interp_offset, quad);

  vec2 final_pos = mix(vec2(ivec2(pos.xy) + ivec2(-interp_size, interp_size)),
                       vec2(ivec2(pos.zw) + ivec2(interp_size, -interp_size)),
                       quad);

  gl_Position = ModelViewProjectionMatrix * vec4(final_pos, 0.0, 1.0);
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define blender_srgb_to_framebuffer_space(a) a 
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D glyph;

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform bool srgbTarget;


/* Interfaces. */
in text_iface{
  flat vec4 color_flat;
  noperspective vec2 texCoord_interp;
  flat int glyph_offset;
  flat ivec2 glyph_dim;
  flat int interp_size;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


/* Undefine the macro that avoids compilation errors. */
#undef blender_srgb_to_framebuffer_space

#ifndef USE_GPU_SHADER_CREATE_INFO
uniform bool srgbTarget = false;
#endif

vec4 blender_srgb_to_framebuffer_space(vec4 in_color)
{
  if (srgbTarget) {
    vec3 c = max(in_color.rgb, vec3(0.0));
    vec3 c1 = c * (1.0 / 12.92);
    vec3 c2 = pow((c + 0.055) * (1.0 / 1.055), vec3(2.4));
    in_color.rgb = mix(c1, c2, step(vec3(0.04045), c));
  }
  return in_color;
}
#pragma BLENDER_REQUIRE(gpu_shader_colorspace_lib.glsl)

const vec2 offsets4[4] = vec2[4](
    vec2(-0.5, 0.5), vec2(0.5, 0.5), vec2(-0.5, -0.5), vec2(-0.5, -0.5));

const vec2 offsets16[16] = vec2[16](vec2(-1.5, 1.5),
                                    vec2(-0.5, 1.5),
                                    vec2(0.5, 1.5),
                                    vec2(1.5, 1.5),
                                    vec2(-1.5, 0.5),
                                    vec2(-0.5, 0.5),
                                    vec2(0.5, 0.5),
                                    vec2(1.5, 0.5),
                                    vec2(-1.5, -0.5),
                                    vec2(-0.5, -0.5),
                                    vec2(0.5, -0.5),
                                    vec2(1.5, -0.5),
                                    vec2(-1.5, -1.5),
                                    vec2(-0.5, -1.5),
                                    vec2(0.5, -1.5),
                                    vec2(1.5, -1.5));

//#define GPU_NEAREST
#define sample_glyph_offset(texel, ofs) \
  texture_1D_custom_bilinear_filter(texCoord_interp + ofs * texel)

float texel_fetch(int index)
{
  int size_x = textureSize(glyph, 0).r;
  if (index >= size_x) {
    return texelFetch(glyph, ivec2(index % size_x, index / size_x), 0).r;
  }
  return texelFetch(glyph, ivec2(index, 0), 0).r;
}

bool is_inside_box(ivec2 v)
{
  return all(greaterThanEqual(v, ivec2(0))) && all(lessThan(v, glyph_dim));
}

float texture_1D_custom_bilinear_filter(vec2 uv)
{
  vec2 texel_2d = uv * vec2(glyph_dim) + vec2(0.5);
  ivec2 texel_2d_near = ivec2(texel_2d) - 1;
  int frag_offset = glyph_offset + texel_2d_near.y * glyph_dim.x + texel_2d_near.x;

  float tl = 0.0;

  if (is_inside_box(texel_2d_near)) {
    tl = texel_fetch(frag_offset);
  }

#ifdef GPU_NEAREST
  return tl;
#else  // GPU_LINEAR
  int offset_x = 1;
  int offset_y = glyph_dim.x;

  float tr = 0.0;
  float bl = 0.0;
  float br = 0.0;

  if (is_inside_box(texel_2d_near + ivec2(1, 0))) {
    tr = texel_fetch(frag_offset + offset_x);
  }
  if (is_inside_box(texel_2d_near + ivec2(0, 1))) {
    bl = texel_fetch(frag_offset + offset_y);
  }
  if (is_inside_box(texel_2d_near + ivec2(1, 1))) {
    br = texel_fetch(frag_offset + offset_x + offset_y);
  }

  vec2 f = fract(texel_2d);
  float tA = mix(tl, tr, f.x);
  float tB = mix(bl, br, f.x);

  return mix(tA, tB, f.y);
#endif
}

void main()
{
  // input color replaces texture color
  fragColor.rgb = color_flat.rgb;

  // modulate input alpha & texture alpha
  if (interp_size == 0) {
    fragColor.a = texture_1D_custom_bilinear_filter(texCoord_interp);
  }
  else {
    vec2 texel = 1.0 / vec2(glyph_dim);
    fragColor.a = 0.0;

    if (interp_size == 1) {
      /* 3x3 blur */
      /* Manual unroll for perf. (stupid glsl compiler) */
      fragColor.a += sample_glyph_offset(texel, offsets4[0]);
      fragColor.a += sample_glyph_offset(texel, offsets4[1]);
      fragColor.a += sample_glyph_offset(texel, offsets4[2]);
      fragColor.a += sample_glyph_offset(texel, offsets4[3]);
      fragColor.a *= (1.0 / 4.0);
    }
    else {
      /* 5x5 blur */
      /* Manual unroll for perf. (stupid glsl compiler) */
      fragColor.a += sample_glyph_offset(texel, offsets16[0]);
      fragColor.a += sample_glyph_offset(texel, offsets16[1]);
      fragColor.a += sample_glyph_offset(texel, offsets16[2]);
      fragColor.a += sample_glyph_offset(texel, offsets16[3]);

      fragColor.a += sample_glyph_offset(texel, offsets16[4]);
      fragColor.a += sample_glyph_offset(texel, offsets16[5]) * 2.0;
      fragColor.a += sample_glyph_offset(texel, offsets16[6]) * 2.0;
      fragColor.a += sample_glyph_offset(texel, offsets16[7]);

      fragColor.a += sample_glyph_offset(texel, offsets16[8]);
      fragColor.a += sample_glyph_offset(texel, offsets16[9]) * 2.0;
      fragColor.a += sample_glyph_offset(texel, offsets16[10]) * 2.0;
      fragColor.a += sample_glyph_offset(texel, offsets16[11]);

      fragColor.a += sample_glyph_offset(texel, offsets16[12]);
      fragColor.a += sample_glyph_offset(texel, offsets16[13]);
      fragColor.a += sample_glyph_offset(texel, offsets16[14]);
      fragColor.a += sample_glyph_offset(texel, offsets16[15]);
      fragColor.a *= (1.0 / 20.0);
    }
  }

  fragColor.a *= color_flat.a;
  fragColor = blender_srgb_to_framebuffer_space(fragColor);
}

