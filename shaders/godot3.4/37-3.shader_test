[require]
GLSL >= 3.30

[fragment shader]
#version 330
#define GLES_OVER_GL
#define USE_FRACTIONAL_DELTA
#define USE_MATERIAL
precision highp float;
precision highp int;

// any code here is never executed, stuff is filled just so it works

#if defined(USE_MATERIAL)

layout(std140) uniform UniformData {
vec3 m_direction;
float m_spread;
float m_flatness;
float m_initial_linear_velocity;
float m_initial_angle;
float m_angular_velocity;
float m_orbit_velocity;
float m_linear_accel;
float m_radial_accel;
float m_tangent_accel;
float m_damping;
float m_scale;
float m_hue_variation;
float m_anim_speed;
float m_anim_offset;
float m_initial_linear_velocity_random;
float m_initial_angle_random;
float m_angular_velocity_random;
float m_orbit_velocity_random;
float m_linear_accel_random;
float m_radial_accel_random;
float m_tangent_accel_random;
float m_damping_random;
float m_scale_random;
float m_hue_variation_random;
float m_anim_speed_random;
float m_anim_offset_random;
float m_lifetime_randomness;
int m_emission_texture_point_count;
vec4 m_color_value;
int m_trail_divisor;
vec3 m_gravity;


};

#endif
uniform sampler2D m_emission_texture_points;


void main() {

	{


	}

	{


	}
}
/* clang-format on */

[vertex shader]
#version 330
#define GLES_OVER_GL
#define USE_FRACTIONAL_DELTA
#define USE_MATERIAL
precision highp float;
precision highp int;

layout(location = 0) in highp vec4 color;
/* clang-format on */
layout(location = 1) in highp vec4 velocity_active;
layout(location = 2) in highp vec4 custom;
layout(location = 3) in highp vec4 xform_1;
layout(location = 4) in highp vec4 xform_2;
layout(location = 5) in highp vec4 xform_3;

struct Attractor {
	vec3 pos;
	vec3 dir;
	float radius;
	float eat_radius;
	float strength;
	float attenuation;
};

#define MAX_ATTRACTORS 64

uniform bool emitting;
uniform float system_phase;
uniform float prev_system_phase;
uniform int total_particles;
uniform float explosiveness;
uniform float randomness;
uniform float time;
uniform float delta;

uniform int attractor_count;
uniform Attractor attractors[MAX_ATTRACTORS];
uniform bool clear;
uniform uint cycle;
uniform float lifetime;
uniform mat4 emission_transform;
uniform uint random_seed;

out highp vec4 out_color; //tfb:
out highp vec4 out_velocity_active; //tfb:
out highp vec4 out_custom; //tfb:
out highp vec4 out_xform_1; //tfb:
out highp vec4 out_xform_2; //tfb:
out highp vec4 out_xform_3; //tfb:

#if defined(USE_MATERIAL)

/* clang-format off */
layout(std140) uniform UniformData { //ubo:0
vec3 m_direction;
float m_spread;
float m_flatness;
float m_initial_linear_velocity;
float m_initial_angle;
float m_angular_velocity;
float m_orbit_velocity;
float m_linear_accel;
float m_radial_accel;
float m_tangent_accel;
float m_damping;
float m_scale;
float m_hue_variation;
float m_anim_speed;
float m_anim_offset;
float m_initial_linear_velocity_random;
float m_initial_angle_random;
float m_angular_velocity_random;
float m_orbit_velocity_random;
float m_linear_accel_random;
float m_radial_accel_random;
float m_tangent_accel_random;
float m_damping_random;
float m_scale_random;
float m_hue_variation_random;
float m_anim_speed_random;
float m_anim_offset_random;
float m_lifetime_randomness;
int m_emission_texture_point_count;
vec4 m_color_value;
int m_trail_divisor;
vec3 m_gravity;


};
/* clang-format on */

#endif

/* clang-format off */
uniform sampler2D m_emission_texture_points;

uint m_hash(uint m_x)
	{
		m_x=(((m_x>>16u)^m_x)*73244475u);
		m_x=(((m_x>>16u)^m_x)*73244475u);
		m_x=((m_x>>16u)^m_x);
return m_x;	}

float m_rand_from_seed(inout uint m_seed)
	{
		int m_k;
		int m_s=int(m_seed);
		if ((m_s==0))
		{
			m_s=305420679;
		}
		m_k=(m_s/127773);
		m_s=((16807*(m_s-(m_k*127773)))-(2836*m_k));
		if ((m_s<0))
		{
			m_s+=2147483647;
		}
		m_seed=uint(m_s);
return (float((m_seed%65536u))/65535.0);	}

float m_rand_from_seed_m1_p1(inout uint m_seed)
	{
return ((m_rand_from_seed(m_seed)*2.0)-1.0);	}


/* clang-format on */

uint hash(uint x) {
	x = ((x >> uint(16)) ^ x) * uint(0x45d9f3b);
	x = ((x >> uint(16)) ^ x) * uint(0x45d9f3b);
	x = (x >> uint(16)) ^ x;
	return x;
}

void main() {
#ifdef PARTICLES_COPY

	out_color = color;
	out_velocity_active = velocity_active;
	out_custom = custom;
	out_xform_1 = xform_1;
	out_xform_2 = xform_2;
	out_xform_3 = xform_3;

#else

	bool apply_forces = true;
	bool apply_velocity = true;
	float local_delta = delta;

	float mass = 1.0;

	float restart_phase = float(gl_VertexID) / float(total_particles);

	if (randomness > 0.0) {
		uint seed = cycle;
		if (restart_phase >= system_phase) {
			seed -= uint(1);
		}
		seed *= uint(total_particles);
		seed += uint(gl_VertexID);
		float random = float(hash(seed) % uint(65536)) / 65536.0;
		restart_phase += randomness * random * 1.0 / float(total_particles);
	}

	restart_phase *= (1.0 - explosiveness);
	bool restart = false;
	bool shader_active = velocity_active.a > 0.5;

	if (system_phase > prev_system_phase) {
		// restart_phase >= prev_system_phase is used so particles emit in the first frame they are processed

		if (restart_phase >= prev_system_phase && restart_phase < system_phase) {
			restart = true;
#ifdef USE_FRACTIONAL_DELTA
			local_delta = (system_phase - restart_phase) * lifetime;
#endif
		}

	} else if (delta > 0.0) {
		if (restart_phase >= prev_system_phase) {
			restart = true;
#ifdef USE_FRACTIONAL_DELTA
			local_delta = (1.0 - restart_phase + system_phase) * lifetime;
#endif
		} else if (restart_phase < system_phase) {
			restart = true;
#ifdef USE_FRACTIONAL_DELTA
			local_delta = (system_phase - restart_phase) * lifetime;
#endif
		}
	}

	uint current_cycle = cycle;

	if (system_phase < restart_phase) {
		current_cycle -= uint(1);
	}

	uint particle_number = current_cycle * uint(total_particles) + uint(gl_VertexID);
	int index = int(gl_VertexID);

	if (restart) {
		shader_active = emitting;
	}

	mat4 xform;

#if defined(ENABLE_KEEP_DATA)
	if (clear) {
#else
	if (clear || restart) {
#endif
		out_color = vec4(1.0);
		out_velocity_active = vec4(0.0);
		out_custom = vec4(0.0);
		if (!restart)
			shader_active = false;

		xform = mat4(
				vec4(1.0, 0.0, 0.0, 0.0),
				vec4(0.0, 1.0, 0.0, 0.0),
				vec4(0.0, 0.0, 1.0, 0.0),
				vec4(0.0, 0.0, 0.0, 1.0));
	} else {
		out_color = color;
		out_velocity_active = velocity_active;
		out_custom = custom;
		xform = transpose(mat4(xform_1, xform_2, xform_3, vec4(vec3(0.0), 1.0)));
	}

	if (shader_active) {
		//execute shader

		{
			/* clang-format off */
	{
		uint m_base_number=(particle_number/uint(m_trail_divisor));
		uint m_alt_seed=m_hash(((m_base_number+1u)+random_seed));
		float m_angle_rand=m_rand_from_seed(m_alt_seed);
		float m_scale_rand=m_rand_from_seed(m_alt_seed);
		float m_hue_rot_rand=m_rand_from_seed(m_alt_seed);
		float m_anim_offset_rand=m_rand_from_seed(m_alt_seed);
		float m_pi=3.14159;
		float m_degree_to_rad=(m_pi/180.0);
		int m_point=min((m_emission_texture_point_count-1), int((m_rand_from_seed(m_alt_seed)*float(m_emission_texture_point_count))));
		ivec2 m_emission_tex_size=textureSize(m_emission_texture_points, 0);
		ivec2 m_emission_tex_ofs=ivec2((m_point%m_emission_tex_size.x), (m_point/m_emission_tex_size.x));
		bool m_restart=false;
		float m_tv=0.0;
		if ((out_custom.y>out_custom.w))
		{
					{
			m_restart=true;
			m_tv=1.0;
		}
;
		}
		if ((restart||m_restart))
		{
					{
			uint m_alt_restart_seed=m_hash(((m_base_number+301184u)+random_seed));
			float m_tex_linear_velocity=0.0;
			float m_tex_angle=0.0;
			float m_tex_anim_offset=0.0;
			float m_spread_rad=(m_spread*m_degree_to_rad);
					{
			float m_angle1_rad=(m_rand_from_seed_m1_p1(m_alt_restart_seed)*m_spread_rad);
			m_angle1_rad+=((m_direction.x!=0.0)?atan(m_direction.y, m_direction.x):(sign(m_direction.y)*(m_pi/2.0)));
			vec3 m_rot=vec3(cos(m_angle1_rad), sin(m_angle1_rad), 0.0);
			out_velocity_active.xyz=((m_rot*m_initial_linear_velocity)*mix(1.0, m_rand_from_seed(m_alt_restart_seed), m_initial_linear_velocity_random));
		}
;
			float m_base_angle=((m_initial_angle+m_tex_angle)*mix(1.0, m_angle_rand, m_initial_angle_random));
			out_custom.x=(m_base_angle*m_degree_to_rad);
			out_custom.y=0.0;
			out_custom.w=(1.0-(m_lifetime_randomness*m_rand_from_seed(m_alt_restart_seed)));
			out_custom.z=((m_anim_offset+m_tex_anim_offset)*mix(1.0, m_anim_offset_rand, m_anim_offset_random));
			xform[3].xyz=texelFetch(m_emission_texture_points, m_emission_tex_ofs, 0).xyz;
			out_velocity_active.xyz=(emission_transform*vec4(out_velocity_active.xyz, 0.0)).xyz;
			xform=(emission_transform*xform);
			out_velocity_active.xyz.z=0.0;
			xform[3].z=0.0;
		}
;
		}
		else
		{
					{
			out_custom.y+=(local_delta/lifetime);
			m_tv=(out_custom.y/out_custom.w);
			float m_tex_linear_velocity=0.0;
			float m_tex_orbit_velocity=0.0;
			float m_tex_angular_velocity=0.0;
			float m_tex_linear_accel=0.0;
			float m_tex_radial_accel=0.0;
			float m_tex_tangent_accel=0.0;
			float m_tex_damping=0.0;
			float m_tex_angle=0.0;
			float m_tex_anim_speed=0.0;
			float m_tex_anim_offset=0.0;
			vec3 m_force=m_gravity;
			vec3 m_pos=xform[3].xyz;
			m_pos.z=0.0;
			m_force+=((length(out_velocity_active.xyz)>0.0)?((normalize(out_velocity_active.xyz)*(m_linear_accel+m_tex_linear_accel))*mix(1.0, m_rand_from_seed(m_alt_seed), m_linear_accel_random)):vec3(0.0,0.0,0.0));
			vec3 m_org=emission_transform[3].xyz;
			vec3 m_diff=(m_pos-m_org);
			m_force+=((length(m_diff)>0.0)?((normalize(m_diff)*(m_radial_accel+m_tex_radial_accel))*mix(1.0, m_rand_from_seed(m_alt_seed), m_radial_accel_random)):vec3(0.0,0.0,0.0));
			m_force+=((length(m_diff.yx)>0.0)?(vec3(normalize((m_diff.yx*vec2(-1.0,1.0))), 0.0)*((m_tangent_accel+m_tex_tangent_accel)*mix(1.0, m_rand_from_seed(m_alt_seed), m_tangent_accel_random))):vec3(0.0,0.0,0.0));
			out_velocity_active.xyz+=(m_force*local_delta);
			float m_orbit_amount=((m_orbit_velocity+m_tex_orbit_velocity)*mix(1.0, m_rand_from_seed(m_alt_seed), m_orbit_velocity_random));
			if ((m_orbit_amount!=0.0))
			{
							{
				float m_ang=(((m_orbit_amount*local_delta)*m_pi)*2.0);
				mat2 m_rot=mat2(vec2(cos(m_ang), -sin(m_ang)), vec2(sin(m_ang), cos(m_ang)));
				xform[3].xy-=m_diff.xy;
				xform[3].xy+=(m_rot*m_diff.xy);
			}
;
			}
			if (((m_damping+m_tex_damping)>0.0))
			{
							{
				float m_v=length(out_velocity_active.xyz);
				float m_damp=((m_damping+m_tex_damping)*mix(1.0, m_rand_from_seed(m_alt_seed), m_damping_random));
				m_v-=(m_damp*local_delta);
				if ((m_v<0.0))
				{
									{
					out_velocity_active.xyz=vec3(0.0,0.0,0.0);
				}
;
				}
				else
				{
									{
					out_velocity_active.xyz=(normalize(out_velocity_active.xyz)*m_v);
				}
;
				}
			}
;
			}
			float m_base_angle=((m_initial_angle+m_tex_angle)*mix(1.0, m_angle_rand, m_initial_angle_random));
			m_base_angle+=(((out_custom.y*lifetime)*(m_angular_velocity+m_tex_angular_velocity))*mix(1.0, ((m_rand_from_seed(m_alt_seed)*2.0)-1.0), m_angular_velocity_random));
			out_custom.x=(m_base_angle*m_degree_to_rad);
			out_custom.z=(((m_anim_offset+m_tex_anim_offset)*mix(1.0, m_anim_offset_rand, m_anim_offset_random))+((out_custom.y*(m_anim_speed+m_tex_anim_speed))*mix(1.0, m_rand_from_seed(m_alt_seed), m_anim_speed_random)));
		}
;
		}
		float m_tex_scale=1.0;
		float m_tex_hue_variation=0.0;
		float m_hue_rot_angle=((((m_hue_variation+m_tex_hue_variation)*m_pi)*2.0)*mix(1.0, ((m_hue_rot_rand*2.0)-1.0), m_hue_variation_random));
		float m_hue_rot_c=cos(m_hue_rot_angle);
		float m_hue_rot_s=sin(m_hue_rot_angle);
		mat4 m_hue_rot_mat=((mat4(0.299,0.587,0.114,0.0,0.299,0.587,0.114,0.0,0.299,0.587,0.114,0.0,0.0,0.0,0.0,1.0)+(mat4(0.701,-0.587,-0.114,0.0,-0.299,0.413,-0.114,0.0,-0.3,-0.588,0.886,0.0,0.0,0.0,0.0,0.0)*m_hue_rot_c))+(mat4(0.168,0.33,-0.497,0.0,-0.328,0.035,0.292,0.0,1.25,-1.05,-0.203,0.0,0.0,0.0,0.0,0.0)*m_hue_rot_s));
		out_color=(m_hue_rot_mat*m_color_value);
		xform[0]=vec4(cos(out_custom.x), -sin(out_custom.x), 0.0, 0.0);
		xform[1]=vec4(sin(out_custom.x), cos(out_custom.x), 0.0, 0.0);
		xform[2]=vec4(0.0,0.0,1.0,0.0);
		float m_base_scale=(m_tex_scale*mix(m_scale, 1.0, (m_scale_random*m_scale_rand)));
		if ((m_base_scale<1e-06))
		{
					{
			m_base_scale=1e-06;
		}
;
		}
		xform[0].xyz*=m_base_scale;
		xform[1].xyz*=m_base_scale;
		xform[2].xyz*=m_base_scale;
		out_velocity_active.xyz.z=0.0;
		xform[3].z=0.0;
		if ((out_custom.y>out_custom.w))
		{
					{
			shader_active=false;
		}
;
		}
	}


			/* clang-format on */
		}

#if !defined(DISABLE_FORCE)

		if (false) {
			vec3 force = vec3(0.0);
			for (int i = 0; i < attractor_count; i++) {
				vec3 rel_vec = xform[3].xyz - attractors[i].pos;
				float dist = length(rel_vec);
				if (attractors[i].radius < dist)
					continue;
				if (attractors[i].eat_radius > 0.0 && attractors[i].eat_radius > dist) {
					out_velocity_active.a = 0.0;
				}

				rel_vec = normalize(rel_vec);

				float attenuation = pow(dist / attractors[i].radius, attractors[i].attenuation);

				if (attractors[i].dir == vec3(0.0)) {
					//towards center
					force += attractors[i].strength * rel_vec * attenuation * mass;
				} else {
					force += attractors[i].strength * attractors[i].dir * attenuation * mass;
				}
			}

			out_velocity_active.xyz += force * local_delta;
		}
#endif

#if !defined(DISABLE_VELOCITY)

		if (true) {
			xform[3].xyz += out_velocity_active.xyz * local_delta;
		}
#endif
	} else {
		xform = mat4(0.0);
	}

	xform = transpose(xform);

	out_velocity_active.a = mix(0.0, 1.0, shader_active);

	out_xform_1 = xform[0];
	out_xform_2 = xform[1];
	out_xform_3 = xform[2];

#endif //PARTICLES_COPY
}

/* clang-format off */

